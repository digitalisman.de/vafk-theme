<?php
defined('TYPO3_MODE') or die();

$GLOBALS['TCA']['sys_category']['ctrl']['sortby'] = null;
$GLOBALS['TCA']['sys_category']['ctrl']['default_sortby'] = 'ORDER BY title';
