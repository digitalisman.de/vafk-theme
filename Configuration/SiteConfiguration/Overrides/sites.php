<?php
// placed in EXT:site/Configuration/SiteConfiguration/Overrides/
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function ($table) {
        $GLOBALS['SiteConfiguration'][$table]['columns']['googleTagManager'] = [
            'label' => 'Google Tag Manager',
            'config' => [
                'type' => 'input',
                'placeholder' => 'GTM-123456',
            ],
        ];
        $GLOBALS['SiteConfiguration'][$table]['columns']['googleAnalytics'] = [
            'label' => 'Google Analytics',
            'config' => [
                'type' => 'input',
                'placeholder' => 'UA-86284974-4',
            ],
        ];
        $GLOBALS['SiteConfiguration'][$table]['columns']['matomo'] = [
            'label' => 'Matomo Seiten ID',
            'config' => [
                'type' => 'input',
                'placeholder' => '5',
            ],
        ];
        $GLOBALS['SiteConfiguration'][$table]['columns']['logofilename'] = [
            'label' => 'Logo Name',
            'config' => [
                'type' => 'input',
                'placeholder' => 'pfad zum Logo aus Filelist',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['columns']['footermenuid'] = [
            'label' => 'Footer Menü ID',
            'config' => [
                'type' => 'input',
                'placeholder' => '2',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['columns']['metamenuid'] = [
            'label' => 'Meta Menü ID',
            'config' => [
                'type' => 'input',
                'placeholder' => '3426',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['columns']['companyname'] = [
            'label' => 'Company Name',
            'config' => [
                'type' => 'input',
                'placeholder' => 'Company Name',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['columns']['address'] = [
            'label' => 'Address',
            'config' => [
                'type' => 'input',
                'placeholder' => 'address',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['columns']['zip'] = [
            'label' => 'Zip',
            'config' => [
                'type' => 'input',
                'placeholder' => 'zip',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['columns']['city'] = [
            'label' => 'City',
            'config' => [
                'type' => 'input',
                'placeholder' => 'city',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['columns']['email'] = [
            'label' => 'Email',
            'config' => [
                'type' => 'input',
                'placeholder' => 'email',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['columns']['phone'] = [
            'label' => 'Phone',
            'config' => [
                'type' => 'input',
                'placeholder' => 'phone',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['columns']['fax'] = [
            'label' => 'Fax',
            'config' => [
                'type' => 'input',
                'placeholder' => 'fax',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['columns']['opentimes'] = [
            'label' => 'Opentimes',
            'config' => [
                'type' => 'input',
                'placeholder' => 'opentimes',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['columns']['ceo'] = [
            'label' => 'Ceo',
            'config' => [
                'type' => 'input',
                'placeholder' => 'ceo',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['columns']['taxid'] = [
            'label' => 'Taxid',
            'config' => [
                'type' => 'input',
                'placeholder' => 'taxid',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['columns']['registrationnumber'] = [
            'label' => 'Registrationnumber',
            'config' => [
                'type' => 'input',
                'placeholder' => 'registrationnumber',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['columns']['registrationcourt'] = [
            'label' => 'Registrationcourt',
            'config' => [
                'type' => 'input',
                'placeholder' => 'registrationcourt',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['columns']['facebook'] = [
            'label' => 'Facebook',
            'config' => [
                'type' => 'input',
                'placeholder' => 'facebook url',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['columns']['twitter'] = [
            'label' => 'Twitter',
            'config' => [
                'type' => 'input',
                'placeholder' => 'twitter url',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['columns']['instagram'] = [
            'label' => 'Instagram',
            'config' => [
                'type' => 'input',
                'placeholder' => 'instagram url',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['columns']['linkedin'] = [
            'label' => 'Linked In',
            'config' => [
                'type' => 'input',
                'placeholder' => 'linkedin url',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['columns']['youtube'] = [
            'label' => 'YouTube',
            'config' => [
                'type' => 'input',
                'placeholder' => 'youtube url',
            ],
        ];

        $GLOBALS['SiteConfiguration'][$table]['types']['0']['showitem'] .= ',--div--;Company,logofilename,metamenuid,footermenuid,logo,companyname,address,zip,city,email,phone,fax,opentimes,ceo,taxid,registrationnumber,registrationcourt,--div--;Google,googleTagManager,googleAnalytics,matomo,--div--;Social,facebook,instagram,twitter,linkedin,youtube';
    },
    'site'
);
