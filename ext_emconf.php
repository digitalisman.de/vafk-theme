<?php

/**
 * Extension Manager/Repository config file for ext "vafk_base_fluid".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'VAFK Base Fluid',
    'description' => '',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '10.2.0-10.4.99',
            'fluid_styled_content' => '10.2.0-10.4.99',
            'rte_ckeditor' => '10.2.0-10.4.99',
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Digitalisman\\VafkBaseFluid\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Daniel Vogel',
    'author_email' => 'info@digitalisman.de',
    'author_company' => 'Digitalisman',
    'version' => '1.0.0',
];
